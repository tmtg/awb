sel_pat_0 = uigetdir;      % Select the pictures folder path
addpath(sel_pat_0);
fol_lis =  dir(fullfile(sel_pat_0,'*.MOV')); 
fileName = fol_lis.name; 
obj = VideoReader(fileName); 
numFrames = obj.NumberOfFrames; 
for k = 1 : numFrames 
     frame = read(obj,k); 
     imwrite(frame,strcat(sel_pat_0,'\',num2str(k),'.jpg'),'jpg'); 
end