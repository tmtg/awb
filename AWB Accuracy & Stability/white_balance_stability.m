%% Start
clc;clear;
% Get the RGB reference value
rgb2lab = makecform('srgb2lab', 'AdaptedWhitePoint', whitepoint('d65'));
ref_rgb = round(load('mcc_rgb.dat').*255);
ref_lab =  applycform(load('mcc_rgb.dat'), rgb2lab);
ref_lab_18gray = ref_lab(22,:);

folder_path = uigetdir;      % Select the pictures folder path
addpath(folder_path);

img_path_list =  dir(fullfile(folder_path,'*.jpg'));       % Find the *.jpg file
img_num = length(img_path_list);               % Count how many '*.jpg' files
l_value = cell(img_num,1);
deltaE_value = cell(img_num,1); deltaL_value = cell(img_num,1);
deltaC_value = cell(img_num,1); deltah_value = cell(img_num,1);
a_value = cell(img_num,1);      b_value = cell(img_num,1);
awb_value = cell(img_num,1);

imgname = img_path_list(1).name;
rect_image = imread(imgname);
[image2,rect] = imcrop(rect_image);% Returns the cropping rectangle in rect, a four-element position vector.

if img_num > 0
    for i = 1:img_num
        image_name = img_path_list(i).name; 
        image = imread(image_name);
        im2 = imcrop(image,rect);% Crops the image,
        % Rect is a four-element position vector that specifies the size and position of the crop rectangle.
        r = round(mean(mean(im2(:,:,1))));
        g = round(mean(mean(im2(:,:,2))));
        b = round(mean(mean(im2(:,:,3))));
        rgb = [r,g,b];
        lab = applycform(rgb/255, rgb2lab);
        max_value = max([r,g,b]);
        min_value = min([r,g,b]);
        awb = (max_value-min_value)/max_value;
        awb_1 = roundn(awb,-4);
        
        deltaE = roundn(sqrt(sum((lab-ref_lab_18gray).^2, 2)),-4);
        deltaL = roundn(abs(lab(:,1)-ref_lab_18gray(:,1)),-4);
        deltaC = roundn(sqrt(deltaE.^2-deltaL.^2),-4);
        deltah = roundn(((atan(lab(:,3)./lab(:,2)) - atan(ref_lab_18gray(:,3)./ref_lab_18gray(:,2)))*180)./pi,-4);
        
        l_data = {lab(:,1)};
        l_value(i,:) = l_data;
        a_data = {lab(:,2)};
        a_value(i,:) = a_data;
        b_data = {lab(:,3)};
        b_value(i,:) = b_data;
        deltaE_data = {deltaE};
        deltaE_value(i,:) = deltaE_data;
        deltaL_data = {deltaL};
        deltaL_value(i,:) = deltaL_data;
        deltaC_data = {deltaC};
        deltaC_value(i,:) = deltaC_data;
        deltah_data = {deltah};
        deltah_value(i,:) = deltah_data;
        awb_error_results = {awb_1};
        awb_value(i,:) = awb_error_results;
    end
end
    Color_temp = {'D50';'D55';'D65';'D75';'F2(CWF)';'F11(TL84)';'F12(U30)';'A'};
    group_illuminant = length(Color_temp);
    test_times = img_num/group_illuminant;
    column_test = 1:group_illuminant:img_num;
    

    excel_data(:,1) = Color_temp;
    
    ii = 1:group_illuminant:img_num;
    jj = 2:test_times+1;
    for k = 1:length(jj)
        excel_data(:,jj(k)) = deltaC_value(ii(k):ii(k)+group_illuminant-1,:);
    end

    xlswrite(strcat(folder_path,'\','AWB stability_results.xlsx'),excel_data,1);
    
    filespec_user=strcat(folder_path,'\','\AWB stability_results.xlsx');
    excel = actxserver('Excel.Application');
    excel.visible = 1;
    workbooks = excel.Workbooks;
    workbook = workbooks.Open (filespec_user);
    
    %%
    chart = excel.ActiveSheet.Shapes.AddChart();
    chart.Name = 'AWB Stability #1';
    ExpChart = excel.ActiveSheet.ChartObjects('AWB Stability #1');
    ExpChart.Activate;
    
    excel.ActiveChart.ChartType = 'xlColumnClustered'; 
    excel.ActiveChart.HasTitle = 1;
    excel.ActiveChart.ChartTitle.Characters.Text = 'AWB Stability #1';
    
    for i_delete = 1:length(jj)+1
      try
        Series = invoke(excel.ActiveChart,'SeriesCollection',1);
        invoke(Series,'Delete');
      catch e
      end
    end
    
    for jj1 = 2:test_times+1;
        NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
        NewSeries.XValues = ['Sheet1' '!A' int2str(1) ':A' int2str(5)];
        NewSeries.Values  = ['Sheet1' '!' char(64+jj1) int2str(1) ':' char(64+jj1) int2str(5)];
    end
    
    % Setting the (X-Axis) and (Y-Axis) titles. 
    ChartAxes = chart.Chart.Axes(1); 
    set(ChartAxes,'HasTitle',1); 
    set(ChartAxes.AxisTitle,'Caption','illuminants'); 
    ChartAxes = chart.Chart.Axes(2);  
    set(ChartAxes,'HasTitle',1); 
    set(ChartAxes.AxisTitle,'Caption','delta C error'); 
    
    % Setting the (Axis) Scale 
    excel.ActiveChart.Axes(2).Select; 
    excel.ActiveChart.Axes(2).MinimumScale = 0;             
    excel.ActiveChart.Axes(2).MaximumScale = 6;
    excel.ActiveChart.Axes(2).MajorUnit = 1;
    
    % Setting the chart size    
    excel.ActiveChart.ChartArea.Width = 500;
    excel.ActiveChart.ChartArea.Height = 250;
    
    % Placement
    GetPlacement = get(excel.ActiveSheet,'Range', 'A9');
    ExpChart.Left = GetPlacement.Left;
    ExpChart.Top = GetPlacement.Top;
    
    %%
    chart = excel.ActiveSheet.Shapes.AddChart();
    chart.Name = 'AWB Stability #2';
    ExpChart = excel.ActiveSheet.ChartObjects('AWB Stability #2');
    ExpChart.Activate;
    
    excel.ActiveChart.ChartType = 'xlColumnClustered'; 
    excel.ActiveChart.HasTitle = 1;
    excel.ActiveChart.ChartTitle.Characters.Text = 'AWB Stability #2';
    
    for ii2 = 1:length(jj)+1
      try
        Series = invoke(excel.ActiveChart,'SeriesCollection',1);
        invoke(Series,'Delete');
      catch e
      end
    end
    
    for jj2 = 2:test_times+1;
        NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
        NewSeries.XValues = ['Sheet1' '!A' int2str(6) ':A' int2str(8)];
        NewSeries.Values  = ['Sheet1' '!' char(64+jj2) int2str(6) ':' char(64+jj2) int2str(8)];
    end
    
    % Setting the (X-Axis) and (Y-Axis) titles. 
    ChartAxes = chart.Chart.Axes(1); 
    set(ChartAxes,'HasTitle',1); 
    set(ChartAxes.AxisTitle,'Caption','illuminants'); 
    ChartAxes = chart.Chart.Axes(2);  
    set(ChartAxes,'HasTitle',1); 
    set(ChartAxes.AxisTitle,'Caption','delta C error'); 
    
    % Setting the (Axis) Scale 
    excel.ActiveChart.Axes(2).Select; 
    excel.ActiveChart.Axes(2).MinimumScale = 10;             
    
    % Setting the chart size    
    excel.ActiveChart.ChartArea.Width = 400;
    excel.ActiveChart.ChartArea.Height = 250;
    
    % Placement
    GetPlacement = get(excel.ActiveSheet,'Range', 'K9');
    ExpChart.Left = GetPlacement.Left;
    ExpChart.Top = GetPlacement.Top;
    
    workbook.Save();
    workbook.Close();
    excel.Quit();
    disp('Done!');