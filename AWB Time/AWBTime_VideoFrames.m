sel_pat_0 = uigetdir;      % Select the pictures folder path
addpath(sel_pat_0);
fol_lis =  dir(fullfile(sel_pat_0,'*.MOV')); 
fileName = fol_lis.name; 
obj = VideoReader(fileName); 
numFrames = obj.NumberOfFrames; 
for k = 1 : numFrames 
     frame = read(obj,k); 
     imwrite(frame,strcat(sel_pat_0,'\',num2str(k),'.jpg'),'jpg'); 
end

rgb2lab = makecform('srgb2lab', 'AdaptedWhitePoint', whitepoint('d65'));
ref_rgb = round(load('mcc_rgb.dat').*255);
ref_lab =  applycform(load('mcc_rgb.dat'), rgb2lab);
ref_lab_18gray = ref_lab(22,:);

addpath(sel_pat_0);
img_folder = dir(fullfile(sel_pat_0,'*.jpg'));
img_num = length(img_folder);
rg_value = cell(img_num,1);
bg_value = cell(img_num,1);
lab_value = cell(img_num,1);
deltaE_value = cell(img_num,1); deltaL_value = cell(img_num,1);
deltaC_value = cell(img_num,1); deltaH_value = cell(img_num,1);
awb_value = cell(img_num,1);

img_capture = img_folder(1).name;
rect_image = imread(img_capture);
[image2,rect] = imcrop(rect_image);

if img_num > 0
    for i = 1:img_num
        image_name = img_folder(i).name; 
        image = imread(image_name);
        im2 = imcrop(image,rect); % Crops the image,
        % Rect is a four-element position vector that specifies the size and position of the crop rectangle.
        r = round(mean(mean(im2(:,:,1))));
        g = round(mean(mean(im2(:,:,2))));
        b = round(mean(mean(im2(:,:,3))));
        rgb = [r,g,b];
        lab = applycform(rgb/255, rgb2lab);
        max_value = max([r,g,b]);
        min_value = min([r,g,b]);
        awb = (max_value-min_value)/max_value;
        awb_1 = roundn(awb,-4); % AWB error value
        
        deltaE = roundn(sqrt(sum((lab-ref_lab_18gray).^2, 2)),-4); % delta E
        deltaL = roundn(abs(lab(:,1)-ref_lab_18gray(:,1)),-4);   % delta L
        deltaC = roundn(sqrt(deltaE.^2-deltaL.^2),-4);           % delta C
        deltaH = roundn((abs(atan(lab(:,3)./lab(:,2)) - atan(ref_lab_18gray(:,3)./ref_lab_18gray(:,2)))*180)./pi,-4);
                                                                 % delta H
        rg = mean(r./g);
        bg = mean(b./g);
        rg_data = {rg};
        rg_value(i,:) = rg_data;
        bg_data = {bg};
        bg_value(i,:) = bg_data;
        lab_data = {lab};
        lab_value(i,:) = lab_data;
        deltaE_data = {deltaE};
        deltaE_value(i,:) = deltaE_data;
        deltaL_data = {deltaL};
        deltaL_value(i,:) = deltaL_data;
        deltaC_data = {deltaC};
        deltaC_value(i,:) = deltaC_data;
        deltaH_data = {deltaH};
        deltaH_value(i,:) = deltaH_data;
        awb_error_results = {awb_1};
        awb_value(i,:) = awb_error_results;   % Collect all data
    end
end
b = 1:img_num;
excel_data = cell(img_num,4);
excel_data(:,1) = num2cell(b);
excel_data(:,2) = rg_value;
excel_data(:,3) = bg_value;
excel_data(:,4) = deltaC_value;
excel_title = {'Frames','R/G','B/G','Delta C'};
excel = [excel_title;excel_data];
xlswrite(strcat(sel_pat_0,'\','AWB Time.xlsx'),excel,1);

%%
filespec_user=strcat(sel_pat_0,'\','AWB Time.xlsx');
excel = actxserver('Excel.Application');
excel.visible = 1;
workbooks = excel.Workbooks;
workbook = workbooks.Open (filespec_user);

chart = excel.ActiveSheet.Shapes.AddChart();
chart.Name = 'AWB Performance';
ExpChart = excel.ActiveSheet.ChartObjects('AWB Performance');
ExpChart.Activate;

excel.ActiveChart.ChartType = 'xlXYScatterLines'; 
excel.ActiveChart.HasTitle = 1;
excel.ActiveChart.ChartTitle.Characters.Text = 'AWB Performance';
 
for i_delete = 1:4
  try
    Series = invoke(excel.ActiveChart,'SeriesCollection',1);
    invoke(Series,'Delete');
  catch e
  end
end
NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
NewSeries.XValues = ['Sheet1' '!A' int2str(1) ':A' int2str(img_num+1)];
NewSeries.Values  = ['Sheet1' '!B' int2str(1) ':B' int2str(img_num+1)];
NewSeries.Name    = 'R/G Value';

NewSeries = invoke(excel.ActiveChart.SeriesCollection,'NewSeries');
NewSeries.XValues = ['Sheet1' '!A' int2str(1) ':A' int2str(img_num+1)];
NewSeries.Values  = ['Sheet1' '!C' int2str(1) ':C' int2str(img_num+1)];
NewSeries.Name    = 'B/G Value';

% Setting the (X-Axis) and (Y-Axis) titles. 
ChartAxes = chart.Chart.Axes(1); 
set(ChartAxes,'HasTitle',1); 
set(ChartAxes.AxisTitle,'Caption','Frames'); 
ChartAxes = chart.Chart.Axes(2);  
set(ChartAxes,'HasTitle',1); 
set(ChartAxes.AxisTitle,'Caption','Value'); 

% Setting the chart size    
excel.ActiveChart.ChartArea.Width = 1500;
excel.ActiveChart.ChartArea.Height = 400;

% Placement
GetPlacement = get(excel.ActiveSheet,'Range', 'E1');
ExpChart.Left = GetPlacement.Left;
ExpChart.Top = GetPlacement.Top;

workbook.Save();
workbook.Close();
excel.Quit();
disp('Done!');